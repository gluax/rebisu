use std::{fs, path::Path};

use magic_crypt::{new_magic_crypt, MagicCryptTrait};
use rebisu_common::TraderBackend;
use rebisu_errors::{ConfigError, Result};
use serde::{Deserialize, Serialize};
use toml::de::Error;

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct TraderSettings {
    pub proxy:    Option<String>,
    pub username: String,
    pub password: String,
    pub qr_code:  Option<String>,
}

impl TraderSettings {
    pub fn load(path: &Path, trader: &TraderBackend, encryption_key: &str) -> Result<Self> {
        let content =
            fs::read_to_string(path).map_err(|_| ConfigError::failed_to_read_config_file(&path))?;
        let toml = toml::from_str(&content);

        let mut settings: TraderSettings =
            toml.map_err(|err: Error| ConfigError::invalid_config_file(path, err))?;

        let mc = new_magic_crypt!(encryption_key, 256);

        // TODO: handle unwraps
        settings.username = mc.decrypt_base64_to_string(settings.username).unwrap();
        settings.password = mc.decrypt_base64_to_string(settings.password).unwrap();

        if trader.should_have_qr_code() {
            settings
                .qr_code
                .as_ref()
                .map(|qr_code| mc.decrypt_base64_to_string(qr_code));
        }

        Ok(settings)
    }

    pub fn write(
        path: &Path,
        trader: TraderBackend,
        encryption_key: &str,
        username: &str,
        password: &str,
        qr_code: Option<&String>,
    ) -> Result<()> {
        let mut settings = if path.exists() {
            Self::load(path, &trader, encryption_key)?
        } else {
            Self::default()
        };

        let mc = new_magic_crypt!(encryption_key, 256);

        settings.username = mc.encrypt_str_to_base64(&username);
        settings.password = mc.encrypt_str_to_base64(&password);

        if trader.should_have_qr_code() {
            settings.qr_code = qr_code
                .as_ref()
                .map(|qr_code| mc.encrypt_str_to_base64(qr_code));
        }

        // TODO: handle unwrap
        fs::write(&path, toml::to_string_pretty(&settings).unwrap()).expect("failed to write");
        Ok(())
    }
}
