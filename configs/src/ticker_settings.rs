use std::{fs, path::Path};

use rebisu_common::{FormatValidator, Interval, Period, RunInterval, TickerBackend, TraderBackend};
use rebisu_errors::{ConfigError, Result};
use serde::{Deserialize, Serialize};
use toml::de::Error;

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct TickerSettings {
    pub ticker_backend:     TickerBackend,
    pub run_interval:       RunInterval,
    pub data_period:        Period,
    pub data_interval:      Interval,
    pub buy_sell_script:    String,
    pub trader_backend:     TraderBackend,
    pub initial_investment: u64,
    pub proxy:              Option<String>,
}

impl TickerSettings {
    pub fn load(path: &Path) -> Result<Self> {
        let content =
            fs::read_to_string(path).map_err(|_| ConfigError::failed_to_read_config_file(&path))?;
        let toml = toml::from_str(&content);

        let settings: TickerSettings =
            toml.map_err(|err: Error| ConfigError::invalid_config_file(path, err))?;
        settings.run_interval.validate()?;
        settings.data_period.validate()?;
        settings.data_interval.validate()?;

        Ok(settings)
    }

    pub fn create_template(path: &Path) -> Result<()> {
        fs::create_dir_all(path)
            .map_err(|_| ConfigError::failed_to_create_application_directory(path))?;

        let template = path.join("_template.toml");
        let content = toml::to_string_pretty(&Self::default())
            .map_err(ConfigError::failed_to_stringify_template)?;

        fs::write(&template, content)
            .map_err(|err| ConfigError::failed_to_write_template(template, err))
    }
}
