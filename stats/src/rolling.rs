use std::fmt::Debug;

pub trait ToF64 {
    fn to_f64(&self) -> f64;
}

#[derive(Debug)]
pub struct Roll<T>(pub Vec<Vec<T>>);

impl<T> Iterator for Roll<T> {
    type Item = Vec<T>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.0.is_empty() {
            None
        } else {
            Some(self.0.remove(0))
        }
    }
}

pub trait Rolling<T> {
    fn rolling(&self, window: usize) -> Roll<T>;
}

impl<T> Rolling<T> for Vec<T>
where
    T: Clone + Debug,
{
    fn rolling(&self, window: usize) -> Roll<T> {
        let mut outer: Vec<Vec<T>> = Vec::new();
        let mut numbers = Vec::new();

        for number in self.iter() {
            numbers.push(number.clone());

            if numbers.len() > window {
                numbers.remove(0);
            }

            let temp: Vec<T> = if numbers.len() < window {
                Vec::with_capacity(window)
            } else {
                numbers.clone()
            };

            outer.push(temp);
        }

        Roll(outer)
    }
}

impl<T> Rolling<T> for Vec<Option<T>>
where
    T: Clone + Debug,
{
    fn rolling(&self, window: usize) -> Roll<T> {
        let mut outer: Vec<Vec<T>> = Vec::new();
        let mut numbers = Vec::new();

        for number in self.iter() {
            if let Some(number) = number {
                numbers.push(number.clone());
            } else {
                outer.push(Vec::with_capacity(window));
                continue;
            };

            if numbers.len() > window {
                numbers.remove(0);
            }

            let temp: Vec<T> = if numbers.len() < window {
                Vec::with_capacity(window)
            } else {
                numbers.clone()
            };

            outer.push(temp);
        }

        Roll(outer)
    }
}

pub trait RollingMean<T> {
    fn mean(self) -> Vec<f64>;
}

impl<T> RollingMean<T> for Roll<T>
where
    T: ToF64,
{
    fn mean(self) -> Vec<f64> {
        self.map(|inner| inner.iter().map(|t| t.to_f64()).sum::<f64>() / inner.len() as f64)
            .collect()
    }
}

impl RollingMean<f64> for Roll<f64> {
    fn mean(self) -> Vec<f64> {
        self.map(|inner| inner.iter().copied().sum::<f64>() / inner.len() as f64)
            .collect()
    }
}
