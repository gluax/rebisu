use std::{
    error::Error,
    fmt::{Debug, Display},
};

use super::create_errors;

create_errors!(
    TraderError,
    exit_code_mask: 3000,
    error_code_prefix: "TDR",

    /// When the user is not logged in.
    failed_to_complete_action_not_logged_in {
        args: (trader_service: impl Display, action: impl Display),
        msg: format!("`{}` trader is not currently logged in and could not complete action `{}`.", trader_service, action),
        fatal: false,
        help: None,
    }

    /// When the proxy is invalid or could not be applied.
    failed_to_apply_proxy {
        args: (trader_service: impl Display, proxy: impl Display, err: impl Error),
        msg: format!("`{}` trader failed to set proxy `{}` with err: `{}`.", trader_service, proxy, err),
        fatal: false,
        help: None,
    }

    /// When request client failed to be built.
    failed_to_build_request_client {
        args: (trader_service: impl Display, err: impl Error),
        msg: format!("`{}` trader failed to build request client with err: `{}`.", trader_service, err),
        fatal: false,
        help: None,
    }
);
