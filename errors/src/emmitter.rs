use super::{RebisuError, Result};

#[derive(Debug, Default)]
pub struct ErrorHandler {
    count:   usize,
    emitter: StderrEmitter,
}

impl ErrorHandler {
    pub fn emit_err_from_res<T: Default>(&mut self, res: Result<T>) -> T {
        match res {
            Ok(inner) => inner,
            Err(err) => {
                self.emit_err(err);

                Default::default()
            }
        }
    }

    /// Emit the error `err`.

    pub fn emit_err(&mut self, err: RebisuError) {
        self.count = self.count.saturating_add(1);
        self.emitter.emit_err(err);
    }
}

#[derive(Debug, Default)]
struct StderrEmitter;

impl StderrEmitter {
    fn emit_err(&mut self, err: RebisuError) {
        if err.is_fatal() {
            std::process::exit(err.exit_code());
        }

        eprintln!("{}", err);
    }
}
