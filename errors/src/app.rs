use std::{
    error::Error,
    fmt::{Debug, Display},
};

use super::create_errors;

create_errors!(
    AppError,
    exit_code_mask: 3000,
    error_code_prefix: "APP",

    /// When the specified ticker backend is invalid.
    invalid_ticker_backend {
        args: (backend: impl Display, ticker: impl Display),
        msg: format!("Ticker backend `{}` is set for ticker `{}`, but it is currently not supported.", backend, ticker),
        fatal: false,
        help: None,
    }

    /// When the specified trader backend is invalid.
    invalid_trader_backend {
        args: (backend: impl Display),
        msg: format!("Trader backend is `{}` but it is currently not supported.", backend),
        fatal: false,
        help: None,
    }

    /// When the directory settings could not be found.
    failed_to_find_settings_directory {
        args: (),
        msg: "Failed to find settings directory.".to_string(),
        fatal: true,
        help: None,
    }

    /// When the directory settings could not be accessed.
    failed_to_read_settings_directory {
        args: (err: impl Error),
        msg: format!("Failed to read settings directory with error `{}`.", err),
        fatal: true,
        help: None,
    }

    /// When a directory settings file is invalid.
    invalid_file_found_in_settings_directory {
        args: (err: impl Error),
        msg: format!("Failed to read file in settings directory with error `{}`.", err),
        fatal: false,
        help: None,
    }

    /// When desktop notification creation failed.
    failed_to_create_desktop_notification {
        args: (err: impl Error),
        msg: format!("Failed to create desktop notification with error `{}`.", err),
        fatal: false,
        help: None,
    }

    /// When a ticker failed to fetch its data.
    data_fetch_error {
        args: (ticker: impl Display, backend: impl Display),
        msg: format!("Could not fetch data for ticker `{}` on ticker backend `{}`.", ticker, backend),
        fatal: false,
        help: None,
    }

    /// When the file stem could not be fetched for a config file.
    file_stem_get_error {
        args: (file: impl Debug),
        msg: format!("Could not get the file stem of a config file `{:?}`.", file),
        fatal: false,
        help: None,
    }

    /// When an os string could not be converted to a string.
    failed_to_convert_os_string {
        args: (content: impl Debug),
        msg: format!("Could not convert OS string `{:?}` to a string.", content),
        fatal: false,
        help: None,
    }

    /// When the app could not exit loading state.
    failed_to_exit_load_state {
        args: (err: impl Error),
        msg: format!("Rebisu could not exit the loading state with err `{}`.", err),
        fatal: false,
        help: None,
    }

    /// When the app could not unwrap the trade systems error collector.
    failed_to_read_trader_errors {
        args: (err: impl Debug),
        msg: format!("Rebisu could extract trade error collector with err `{:?}`.", err),
        fatal: false,
        help: None,
    }
);
