mod app;
pub use app::*;

mod config;
pub use config::*;

mod create_error;
pub use create_error::*;

mod emmitter;
pub use emmitter::*;

mod error_code;
pub use error_code::*;

mod script;
pub use script::*;

mod ticker;
pub use ticker::*;

mod trader;
use thiserror::Error;
pub use trader::*;

#[derive(Debug, Error)]
pub enum RebisuError {
    #[error(transparent)]
    ConfigError(#[from] ConfigError),
    #[error(transparent)]
    ScriptError(#[from] ScriptError),
    #[error(transparent)]
    TickerError(#[from] TickerError),
    #[error(transparent)]
    TraderError(#[from] TraderError),
    #[error(transparent)]
    AppError(#[from] AppError),
}

impl RebisuError {
    fn exit_code(&self) -> i32 {
        use RebisuError::*;

        match self {
            AppError(err) => err.exit_code(),
            ConfigError(err) => err.exit_code(),
            ScriptError(err) => err.exit_code(),
            TickerError(err) => err.exit_code(),
            TraderError(err) => err.exit_code(),
        }
    }

    fn is_fatal(&self) -> bool {
        use RebisuError::*;

        match self {
            AppError(err) => err.is_fatal(),
            ConfigError(err) => err.is_fatal(),
            ScriptError(err) => err.is_fatal(),
            TickerError(err) => err.is_fatal(),
            TraderError(err) => err.is_fatal(),
        }
    }
}

pub type Result<T, E = RebisuError> = core::result::Result<T, E>;
