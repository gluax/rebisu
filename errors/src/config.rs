use std::{
    error::Error,
    fmt::{Debug, Display},
};

use super::create_errors;

create_errors!(
    ConfigError,
    exit_code_mask: 0,
    error_code_prefix: "CFG",

    /// When the config file could not be read.
    failed_to_read_config_file {
        args: (path: impl Debug),
        msg: format!("Failed to read config file at `{:?}`.", path),
        fatal: false,
        help: None,
    }

    /// When the config file is invalid.
    invalid_config_file {
        args: (path: impl Debug, err: impl Error),
        msg: format!("Invalid config file at `{:?}` with `{}`.", path, err),
        fatal: false,
        help: None,
    }

    /// When the application directory could not be created.
    failed_to_create_application_directory {
        args: (path: impl Debug),
        msg: format!("Failed to create application directory at `{:?}`.", path),
        fatal: true,
        help: None,
    }

    /// When the template config file could not be stringified.
    failed_to_stringify_template {
        args: (err: impl Error),
        msg: format!("Failed to stringify template config file `{}`.", err),
        fatal: true,
        help: None,
    }

    /// When the template config file could not be written.
    failed_to_write_template {
        args: (path: impl Debug, err: impl Error),
        msg: format!("Failed to write template config file to `{:?}` with `{}`.", path, err),
        fatal: true,
        help: None,
    }

    /// When a config interval is invalid.
    invalid_interval {
        args: (type_: impl Display, measurement: impl Display, max: impl Display, value: impl Display),
        msg: format!("Invalid {} cannot have `{}` > `{}` `{}`.", type_, measurement, max, value),
        fatal: false,
        help: None,
    }

    /// When a config interval section could not be parsed as an int.
    invalid_number {
        args: (type_: impl Display, measurement: impl Display, err: impl Error),
        msg: format!("Invalid {} cannot parse `{}` as u8 with `{}`.", type_, measurement, err),
        fatal: false,
        help: None,
    }

    /// When a config interval section time specifier could not be understood.
    unknown_time_specifier {
        args: (type_: impl Display, specifier: impl Display),
        msg: format!("Invalid {} cannot parse `{}`.", type_, specifier),
        fatal: false,
        help: None,
    }
);
