use std::fmt;

use colored::Colorize;

pub trait ErrorCode: fmt::Display {
    const INDENT: &'static str = "    ";

    /// The ErrorCode which has a default code identifier of 018
    /// This is to make the exit codes unique to Rebisu itself.
    const CODE_IDENTIFIER: i8 = 18;

    /// Returns the error's exit code for the program.
    fn exit_code(&self) -> i32;

    /// Returns the prefixed error identifier.
    fn error_code(&self) -> String;

    /// Returns the error's exit code mask, as to avoid conflicts.
    fn exit_code_mask() -> i32;

    /// Returns the error's code type for the program.
    fn error_type() -> String;

    /// Return's the error's message.
    fn message(&self) -> &str;

    /// Return's the error's help message.
    fn help(&self) -> Option<&str>;

    /// Return's the fatal status of the error.
    fn is_fatal(&self) -> bool;

    fn display(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let error_message = format!(
            "Error [{error_code}]: {message}",
            error_code = self.error_code(),
            message = self.message(),
        );
        write!(f, "{}", error_message.bold().red())?;

        if let Some(help) = self.help() {
            write!(
                f,
                "\n{indent     } |\n\
            {indent     } = {help}",
                indent = Self::INDENT,
                help = help
            )?;
        }

        Ok(())
    }
}
