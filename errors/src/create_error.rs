#[macro_export]
macro_rules! create_errors {
    (@step $code:expr,) => {
        #[inline(always)]
        // Returns the number of unique exit codes that this error type can take on.
        pub fn num_exit_codes() -> i32 {
            $code
        }
    };
    ($(#[$error_type_docs:meta])* $error_type:ident, exit_code_mask: $exit_code_mask:expr, error_code_prefix: $error_code_prefix:expr, $($(#[$docs:meta])* $names:ident { args: ($($arg_names:ident: $arg_types:ty$(,)?)*), msg: $messages:expr, fatal: $fatals:expr, help: $helps:expr, })*) => {
        use crate::{ErrorCode, RebisuError};
        use thiserror::Error;

        // Generates the error Enum.
        #[derive(Debug, Error)]
        $(#[$error_type_docs])*
        pub struct $error_type {
            exit_code: i32,
            message: String,
            help: Option<String>,
            fatal: bool,
        }

        impl std::fmt::Display for $error_type {
            fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
                self.display(f)
            }
        }

        /// Implements the trait for LeoError Codes.
        impl ErrorCode for $error_type {
            #[inline(always)]
            fn exit_code(&self) -> i32 {
                let mut code: i32;
                let code_identifier = Self::CODE_IDENTIFIER;

                if code_identifier > 99 {
                    code = code_identifier as i32 * 100_000;
                } else if code_identifier as i32 > 9 {
                    code = code_identifier as i32 * 10_000;
                } else {
                    code = code_identifier as i32 * 1_000;
                }
                code += self.exit_code;

                code
            }

            #[inline(always)]
            fn error_code(&self) -> String {
                 format!(
                    "E{error_type}{code_identifier:0>3}{exit_code:0>4}",
                    error_type = Self::error_type(),
                    code_identifier = Self::CODE_IDENTIFIER,
                    exit_code = self.exit_code,
                )
            }

            #[inline(always)]
            fn exit_code_mask() -> i32 {
                $exit_code_mask
            }

            #[inline(always)]
            fn error_type() -> String {
                $error_code_prefix.to_string()
            }

            #[inline(always)]
            fn message(&self) -> &str {
                &self.message
            }

            #[inline(always)]
            fn help(&self) -> Option<&str> {
                self.help.as_deref()
            }

            #[inline(always)]
            fn is_fatal(&self) -> bool {
                self.fatal
            }
        }

        // Steps over the list of functions with an initial error code of 0.
        impl $error_type {
            create_errors!(@step 0i32, $(($(#[$docs])* $names($($arg_names: $arg_types,)*), $messages, $fatals, $helps),)*);
        }
    };
    // Creates the error function for the error type.
    (@step $code:expr, ($(#[$error_func_docs:meta])* $error_name:ident($($arg_names:ident: $arg_types:ty,)*), $message:expr, $fatal:expr, $help:expr), $(($(#[$docs:meta])* $names:ident($($tail_arg_names:ident: $tail_arg_types:ty,)*), $messages:expr, $fatals:expr, $helps:expr),)*) => {
        // Formatted errors always takes a span.
        $(#[$error_func_docs])*
        // Expands additional arguments for the error defining function.
        pub fn $error_name($($arg_names: $arg_types,)*) -> RebisuError {
            let error = Self {
                exit_code: $code,
                message: $message,
                help: $help,
                fatal: $fatal,
            };

            error.into()
        }

        // Steps the error code value by one and calls on the rest of the functions.
        create_errors!(@step $code + 1i32, $(($(#[$docs])* $names($($tail_arg_names: $tail_arg_types,)*), $messages, $fatals, $helps),)*);
    };
}
