use std::{
    error::Error,
    fmt::{Debug, Display},
};

use super::create_errors;

create_errors!(
    TickerError,
    exit_code_mask: 2000,
    error_code_prefix: "TKR",

    /// When the proxy is invalid or could not be applied.
    failed_to_apply_proxy {
        args: (ticker_service: impl Display, ticker: impl Display, proxy: impl Display, err: impl Error),
        msg: format!("`{}` ticker `{}` failed to set proxy `{}` with err: `{}`.", ticker_service, ticker, proxy, err),
        fatal: false,
        help: None,
    }

    /// When request client failed to be built.
    failed_to_build_request_client {
        args: (ticker_service: impl Display, ticker: impl Display, err: impl Error),
        msg: format!("`{}` ticker `{}` failed to build request client with err: `{}`.", ticker_service, ticker, err),
        fatal: false,
        help: None,
    }

    /// When request to get the ticker data failed.
    failed_to_fetch_ticker_data {
        args: (ticker_service: impl Display, ticker: impl Display, err: impl Error),
        msg: format!("`{}` ticker `{}` failed to get data with err: `{}`.", ticker_service, ticker, err),
        fatal: false,
        help: None,
    }

    /// Failed to bind ticker data to struct.
    failed_to_format_ticker_data {
        args: (ticker_service: impl Display, ticker: impl Display, err: impl Error),
        msg: format!("`{}` ticker `{}` failed to format data with err: `{}`.", ticker_service, ticker, err),
        fatal: false,
        help: None,
    }

    /// Failed to bind ticker data to struct.
    no_result_data {
        args: (ticker_service: impl Display, ticker: impl Display),
        msg: format!("`{}` ticker `{}` no result data.", ticker_service, ticker),
        fatal: false,
        help: None,
    }

    /// Invalid ticker interval.
    invalid_ticker_interval {
        args: (ticker_service: impl Display, ticker: impl Display, interval: impl Display),
        msg: format!("`{}` ticker `{}` has invalid interval `{}`.", ticker_service, ticker, interval),
        fatal: false,
        help: None,
    }

    /// Invalid ticker period.
    invalid_ticker_period {
        args: (ticker_service: impl Display, ticker: impl Display, period: impl Display),
        msg: format!("`{}` ticker `{}` has invalid period `{}`.", ticker_service, ticker, period),
        fatal: false,
        help: None,
    }
);
