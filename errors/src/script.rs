use std::{
    error::Error,
    fmt::{Debug, Display},
};

use super::create_errors;

create_errors!(
    ScriptError,
    exit_code_mask: 1000,
    error_code_prefix: "SCR",

    /// When a global was failed to be set in the script language.
    failed_to_set_global {
        args: (language: impl Display, global: impl Display, value: impl Display, err: impl Error),
        msg: format!("`{}` failed to set global `{}` with value `{}` and err: `{}`.", language, global, value, err),
        fatal: false,
        help: None,
    }

    /// When a function creation in the script langauge failed.
    failed_to_create_function {
        args: (language: impl Display, function: impl Display, err: impl Error),
        msg: format!("`{}` failed to create function `{}` with err: `{}`.", language, function, err),
        fatal: false,
        help: None,
    }

    /// When a function creation in the script langauge failed.
    failed_to_create_script_instance {
        args: (language: impl Display, err: impl Error),
        msg: format!("`{}` failed to script instance with err: `{}`.", language, err),
        fatal: false,
        help: None,
    }

    /// When a script return value fails to bind to Rust.
    failed_to_create_value_from_script {
        args: (language: impl Display, path: impl Debug, err: impl Error),
        msg: format!("`{}` failed to bind return value from script `{:?}` with err: `{}`.", language, path, err),
        fatal: false,
        help: None,
    }

    /// When a script return value fails to bind to Rust.
    failed_to_create_script_result {
        args: (language: impl Display, path: impl Debug, err: impl Error),
        msg: format!("`{}` failed to create script result from script `{:?}` with err: `{}`.", language, path, err),
        fatal: false,
        help: None,
    }

    /// When a scripting language is unknown.
    unknown_scripting_language {
        args: (path: impl Display),
        msg: format!("Unknown extension on script {}", path),
        fatal: false,
        help: None,
    }
);
