use rebisu_common::ScriptResult;
use rebisu_errors::{Result, ScriptError};

use crate::Script;

pub struct NullScript<'a> {
    path: &'a str,
}

impl<'a> NullScript<'a> {
    pub fn new(path: &'a str) -> Self {
        Self { path }
    }
}

impl<'a> Script for NullScript<'a> {
    fn run<D>(&self, _: D) -> Result<ScriptResult>
    where
        D: std::fmt::Display + 'static,
    {
        Err(ScriptError::unknown_scripting_language(self.path))
    }
}
