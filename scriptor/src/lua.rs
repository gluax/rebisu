use std::{fs::read_to_string, path::Path};

use mlua::{Lua as MLua, LuaOptions, LuaSerdeExt, StdLib, UserData, Value as LuaValue};
use rebisu_common::ScriptResult;
use rebisu_errors::{Result, ScriptError};
use rebisu_stats::{Rolling, RollingMean};

use crate::Script;

pub struct Lua<'a> {
    pub lua:       MLua,
    pub file_name: &'a str,
}

impl<'a> Lua<'a> {
    fn set_global<D>(&self, key: &str, data: D) -> Result<()>
    where
        D: UserData + std::fmt::Display + 'static,
    {
        let globals = self.lua.globals();
        let data_str = data.to_string();

        globals
            .set(key, data)
            .map_err(|e| ScriptError::failed_to_set_global("Lua", key, data_str, e))
    }

    fn set_default_globals(&self) -> Result<()> {
        let globals = self.lua.globals();

        let rolling_mean = self
            .lua
            .create_function(|_, (window, data): (usize, Vec<f64>)| Ok(data.rolling(window).mean()))
            .map_err(|e| ScriptError::failed_to_create_function("Lua", "rolling_mean", e))?;
        let rolling_mean_str = format!("{:?}", rolling_mean);

        globals.set("rolling_mean", rolling_mean).map_err(|e| {
            ScriptError::failed_to_set_global("Lua", "rolling_mean", rolling_mean_str, e)
        })
    }

    pub fn new(file_name: &'a str) -> Result<Self> {
        let lua = Self {
            lua: MLua::new_with(StdLib::MATH, LuaOptions::new())
                .map_err(|e| ScriptError::failed_to_create_script_instance("Lua", e))?,
            file_name,
        };
        lua.set_default_globals()?;

        Ok(lua)
    }
}

impl<'a> Script for Lua<'a> {
    fn run<D>(&self, data: D) -> Result<ScriptResult>
    where
        D: UserData + std::fmt::Display + 'static,
    {
        let path = Path::new("./scripts").join(&self.file_name);

        let lua_code = read_to_string(&path)
            .map_err(|e| ScriptError::failed_to_create_value_from_script("Lua", &path, e))?;

        self.set_global("data", data)?;

        self.lua
            .from_value(
                self.lua.load(&lua_code).eval::<LuaValue>().map_err(|e| {
                    ScriptError::failed_to_create_value_from_script("Lua", &path, e)
                })?,
            )
            .map_err(|e| ScriptError::failed_to_create_script_result("Lua", path, e))
    }
}
