use std::{ffi::OsStr, path::Path};

use mlua::UserData;
use rebisu_common::ScriptResult;
use rebisu_errors::Result;

use crate::{Lua, NullScript};

pub trait Script {
    fn run<D>(&self, data: D) -> Result<ScriptResult>
    where
        D: UserData + std::fmt::Display + 'static;
}

pub enum Scriptor<'a> {
    Lua(Lua<'a>),
    // Js,
    Null(NullScript<'a>),
}

impl<'a> Scriptor<'a> {
    pub fn load(path: &'a Path) -> Result<Self> {
        let extension = path.extension().and_then(OsStr::to_str).unwrap_or_default();
        let file_path_as_str = path.to_str().unwrap_or_default();

        match extension {
            "lua" => Ok(Self::Lua(Lua::new(file_path_as_str)?)),
            // "js"
            // "py"
            _ => Ok(Self::Null(NullScript::new(file_path_as_str))),
        }
    }
}

impl<'a> Script for Scriptor<'a> {
    fn run<D>(&self, data: D) -> Result<ScriptResult>
    where
        D: UserData + std::fmt::Display + 'static,
    {
        use Scriptor::*;

        match self {
            Lua(lua) => lua.run(data),
            // js
            // py
            Null(null) => null.run(data),
        }
    }
}
