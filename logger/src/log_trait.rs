use rebisu_errors::Result;

pub trait Logger<R> {
    fn log(&mut self, record: R) -> Result<()>;
}
