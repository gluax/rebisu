use std::{
    fmt::Debug,
    fs::{self, File},
    path::Path,
};

use csv::{Writer, WriterBuilder};
use rebisu_common::{ScriptResult, TickerBackend, TraderBackend};
use rebisu_errors::Result;
use rebisu_ticker::{Ticker, TickerTrait};
use serde::Serialize;

use super::Logger;

#[derive(Debug, Serialize)]
pub struct TradeLog {
    ticker_name:           &'static str,
    ticker_backend:        TickerBackend,
    trader_backend:        TraderBackend,
    action:                String,
    succesful_transaction: bool,
    data_download_time:    f64,
    script_run_time:       f64,
    trade_run_time:        f64,
    current_total:         u128,
}

impl TradeLog {
    pub fn new(
        ticker: &Ticker,
        ticker_backend: TickerBackend,
        trader_backend: TraderBackend,
        action: ScriptResult,
        data_download_time: f64,
        script_run_time: f64,
    ) -> Self {
        Self {
            ticker_name: ticker.get_ticker_name(),
            ticker_backend,
            trader_backend,
            action: action.to_string(),
            succesful_transaction: true,
            data_download_time,
            script_run_time,
            trade_run_time: 0.0,
            current_total: 0,
        }
    }
}

#[derive(Debug)]
pub struct TradeLogger {
    logger: Writer<File>,
}

impl TradeLogger {
    pub fn new(path: &Path) -> Result<Self> {
        // TODO handle these unwraps.
        let file = fs::OpenOptions::new()
            .write(true)
            .append(true)
            .create(true)
            .open(path)
            .unwrap();
        let metadata = file.metadata().unwrap();

        let csv_writer = WriterBuilder::new()
            .has_headers(metadata.len() == 0)
            .from_writer(file);

        Ok(Self { logger: csv_writer })
    }
}

impl<R: Serialize + Debug> Logger<R> for TradeLogger {
    fn log(&mut self, record: R) -> Result<()> {
        // TODO handle these unwraps.
        self.logger.serialize(record).unwrap();
        self.logger.flush().unwrap();
        Ok(())
    }
}
