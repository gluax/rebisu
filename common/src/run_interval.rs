use std::{fmt, str::FromStr};

use rebisu_errors::{ConfigError, RebisuError, Result};
use serde::{de, Deserialize, Serialize};

use crate::FormatValidator;

///  hour:min:sec
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct RunInterval {
    measurements: [u8; 3],
}

impl RunInterval {
    pub fn new(measurements: [u8; 3]) -> Self {
        Self { measurements }
    }

    pub fn as_f32(&self) -> f32 {
        let mut value = 0.0;

        for (index, measurement) in self.measurements.iter().enumerate() {
            match index {
                0 => value += *measurement as f32 * 3600.0,
                1 => value += *measurement as f32 * 60.0,
                2 => value += *measurement as f32,
                _ => {}
            }
        }

        value
    }
}

impl Default for RunInterval {
    fn default() -> Self {
        Self::new([0, 5, 0])
    }
}

impl<'de> Deserialize<'de> for RunInterval {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?.to_lowercase();

        RunInterval::from_str(&s).map_err(|err| de::Error::custom(err.to_string()))
    }
}

impl fmt::Display for RunInterval {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{:02}:{:02}:{:02}",
            self.measurements[0], self.measurements[1], self.measurements[2]
        )
    }
}

impl FormatValidator for RunInterval {
    const TYPE_: &'static str = "RunInterval";

    fn validate(&self) -> Result<()> {
        for (index, measurement) in self.measurements.iter().enumerate() {
            match index {
                0 if *measurement > 24 => {
                    return Err(ConfigError::invalid_interval(
                        Self::TYPE_,
                        "hours",
                        24,
                        measurement,
                    ));
                }
                1 if *measurement > 60 => {
                    return Err(ConfigError::invalid_interval(
                        Self::TYPE_,
                        "minutes",
                        60,
                        measurement,
                    ));
                }
                2 if *measurement > 60 => {
                    return Err(ConfigError::invalid_interval(
                        Self::TYPE_,
                        "seconds",
                        60,
                        measurement,
                    ));
                }
                _ => {}
            }
        }

        Ok(())
    }
}

impl FromStr for RunInterval {
    type Err = RebisuError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut interval = RunInterval::default();

        for (index, part) in s.split(':').into_iter().enumerate() {
            interval.measurements[index] = u8::from_str(part)
                .map_err(|err| ConfigError::invalid_number("RunInterval", part, err))?;
        }

        Ok(interval)
    }
}

impl Serialize for RunInterval {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}
