use std::fmt;

use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum TraderBackend {
    RobinHood,
    NullBackend(String),
}

impl TraderBackend {
    pub fn should_have_qr_code(&self) -> bool {
        use TraderBackend::*;

        match self {
            RobinHood => true,
            NullBackend(_) => false,
        }
    }
}

impl Default for TraderBackend {
    fn default() -> Self {
        TraderBackend::RobinHood
    }
}

impl<'de> Deserialize<'de> for TraderBackend {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?.to_lowercase();

        Ok(TraderBackend::from(s))
    }
}

impl fmt::Display for TraderBackend {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use TraderBackend::*;

        match self {
            RobinHood => write!(f, "robinhood"),
            NullBackend(backend) => write!(f, "{}", backend),
        }
    }
}

impl From<String> for TraderBackend {
    fn from(item: String) -> Self {
        use TraderBackend::*;

        match item.as_str() {
            "robinhood" => RobinHood,
            _ => NullBackend(item),
        }
    }
}

impl Serialize for TraderBackend {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(self.to_string().as_str())
    }
}
