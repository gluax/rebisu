use std::{fmt, str::FromStr};

use rebisu_errors::{ConfigError, RebisuError, Result};
use regex::Regex;
use serde::{de, Deserialize, Serialize};

use super::FormatValidator;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Period {
    Days(u8),
    Weeks(u8),
    Months(u8),
    Years(u8),
    YTD,
    Max,
}

impl Default for Period {
    fn default() -> Self {
        Self::Weeks(1)
    }
}

impl<'de> Deserialize<'de> for Period {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?.to_lowercase();

        Period::from_str(&s).map_err(|err| de::Error::custom(err.to_string()))
    }
}

impl fmt::Display for Period {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Period::*;

        match self {
            Days(n) => write!(f, "{}d", n),
            Weeks(n) => write!(f, "{}wk", n),
            Months(n) => write!(f, "{}mo", n),
            Years(n) => write!(f, "{}y", n),
            YTD => write!(f, "ytd"),
            Max => write!(f, "max"),
        }
    }
}

impl FormatValidator for Period {
    const TYPE_: &'static str = "Period";

    fn validate(&self) -> Result<()> {
        use Period::*;

        match self {
            Days(n) => Self::validate_days(n),
            Weeks(n) => Self::validate_weeks(n),
            Months(n) => Self::validate_months(n),
            _ => Ok(()),
        }
    }
}

impl FromStr for Period {
    type Err = RebisuError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // TODO: handle unwraps
        let pattern = Regex::new(r"(\d+)").unwrap();
        let found = pattern.find(s).unwrap();

        let (num, measurement) = s.split_at(found.end());
        match measurement.to_ascii_lowercase().as_str() {
            "d" => {
                Ok(Period::Days(num.parse().map_err(|err| {
                    ConfigError::invalid_number("Period", num, err)
                })?))
            }
            "wk" => {
                Ok(Period::Weeks(num.parse().map_err(|err| {
                    ConfigError::invalid_number("Period", num, err)
                })?))
            }
            "mo" => {
                Ok(Period::Months(num.parse().map_err(|err| {
                    ConfigError::invalid_number("Period", num, err)
                })?))
            }
            "y" => {
                Ok(Period::Years(num.parse().map_err(|err| {
                    ConfigError::invalid_number("Period", num, err)
                })?))
            }
            "ytd" => Ok(Period::YTD),
            "max" => Ok(Period::Max),
            _ => Err(ConfigError::unknown_time_specifier("Period", measurement)),
        }
    }
}

impl Serialize for Period {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}
