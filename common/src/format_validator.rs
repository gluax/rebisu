use rebisu_errors::{ConfigError, Result};

pub trait FormatValidator {
    const TYPE_: &'static str;

    fn validate(&self) -> Result<()>;

    fn validate_minutes(minutes: &u8) -> Result<()> {
        if *minutes > 90 {
            Err(ConfigError::invalid_interval(
                Self::TYPE_,
                "minutes",
                60,
                minutes,
            ))
        } else {
            Ok(())
        }
    }

    fn validate_days(days: &u8) -> Result<()> {
        if *days > 7 {
            Err(ConfigError::invalid_interval(Self::TYPE_, "days", 60, days))
        } else {
            Ok(())
        }
    }

    fn validate_weeks(weeks: &u8) -> Result<()> {
        if *weeks > 4 {
            Err(ConfigError::invalid_interval(
                Self::TYPE_,
                "weeks",
                60,
                weeks,
            ))
        } else {
            Ok(())
        }
    }

    fn validate_months(months: &u8) -> Result<()> {
        if *months > 12 {
            Err(ConfigError::invalid_interval(
                Self::TYPE_,
                "months",
                60,
                months,
            ))
        } else {
            Ok(())
        }
    }
}
