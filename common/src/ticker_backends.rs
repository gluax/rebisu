use std::fmt;

use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug)]
pub enum TickerBackend {
    NullBackend,
    Yahoo,
}

impl Default for TickerBackend {
    fn default() -> Self {
        TickerBackend::Yahoo
    }
}

impl<'de> Deserialize<'de> for TickerBackend {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?.to_lowercase();

        Ok(TickerBackend::from(s))
    }
}

impl fmt::Display for TickerBackend {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use TickerBackend::*;

        match self {
            NullBackend => write!(f, "null"),
            Yahoo => write!(f, "yahoo"),
        }
    }
}

impl From<String> for TickerBackend {
    fn from(s: String) -> Self {
        use TickerBackend::*;

        match s.as_str() {
            "yahoo" => Yahoo,
            _ => NullBackend,
        }
    }
}

impl Serialize for TickerBackend {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(self.to_string().as_str())
    }
}
