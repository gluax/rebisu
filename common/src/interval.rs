use std::{fmt, str::FromStr};

use rebisu_errors::{ConfigError, RebisuError, Result};
use regex::Regex;
use serde::{de, Deserialize, Serialize};

use super::FormatValidator;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Interval {
    Minutes(u8),
    Days(u8),
    Weeks(u8),
    Months(u8),
}

impl Default for Interval {
    fn default() -> Self {
        Self::Minutes(5)
    }
}

impl<'de> Deserialize<'de> for Interval {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?.to_lowercase();

        Interval::from_str(&s).map_err(|err| de::Error::custom(err.to_string()))
    }
}

impl fmt::Display for Interval {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Interval::*;

        match self {
            Minutes(n) => write!(f, "{}m", n),
            Days(n) => write!(f, "{}d", n),
            Weeks(n) => write!(f, "{}wk", n),
            Months(n) => write!(f, "{}mo", n),
        }
    }
}

impl FormatValidator for Interval {
    const TYPE_: &'static str = "Interval";

    fn validate(&self) -> Result<()> {
        use Interval::*;

        match self {
            Minutes(n) => Self::validate_minutes(n),
            Days(n) => Self::validate_days(n),
            Weeks(n) => Self::validate_weeks(n),
            Months(n) => Self::validate_months(n),
        }
    }
}

impl FromStr for Interval {
    type Err = RebisuError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // TODO: handle unwraps
        let pattern = Regex::new(r"(\d+)").unwrap();
        let found = pattern.find(s).unwrap();

        let (num, measurement) = s.split_at(found.end());
        match measurement.to_ascii_lowercase().as_str() {
            "m" => Ok(Interval::Minutes(num.parse().map_err(|err| {
                ConfigError::invalid_number("Interval", num, err)
            })?)),
            "d" => Ok(Interval::Days(num.parse().map_err(|err| {
                ConfigError::invalid_number("Interval", num, err)
            })?)),
            "wk" => Ok(Interval::Weeks(num.parse().map_err(|err| {
                ConfigError::invalid_number("Interval", num, err)
            })?)),
            "mo" => Ok(Interval::Months(num.parse().map_err(|err| {
                ConfigError::invalid_number("Interval", num, err)
            })?)),
            _ => Err(ConfigError::unknown_time_specifier("Interval", measurement)),
        }
    }
}

impl Serialize for Interval {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}
