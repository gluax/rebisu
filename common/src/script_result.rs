use std::fmt;

use mlua::UserData;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum ScriptResult {
    Buy(u128),
    Sell(u128),
    Neither,
}

impl fmt::Display for ScriptResult {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use ScriptResult::*;

        match self {
            Buy(amt) => write!(f, "+{}", amt),
            Sell(amt) => write!(f, "-{}", amt),
            Neither => write!(f, "0"),
        }
    }
}

impl UserData for ScriptResult {}
