use rebisu_common::TraderBackend;
use rebisu_configs::TraderSettings;
use rebisu_errors::{Result, TraderError};
use reqwest::{blocking::Client, header::*, Proxy};

use crate::{RHLoginPayload, RHLoginResponse, RHLogoutPayload, TraderTrait};

#[derive(Clone, Debug, Default)]
pub struct RobinhoodTrader {
    settings:      TraderSettings,
    client:        Client,
    headers:       HeaderMap,
    auth_token:    String,
    refresh_token: String,
    client_id:     String,
}

impl RobinhoodTrader {
    const BASE_URL: &'static str = "https://api.robinhood.com";
    const LOGIN: &'static str = "/oauth2/token/";
    const LOGOUT: &'static str = "/oauth2/revoke_token/";

    pub fn new(settings: TraderSettings) -> Result<Self> {
        let client_builder = Client::builder();

        let client = if let Some(proxy) = &settings.proxy {
            client_builder.proxy(Proxy::https(proxy).map_err(|err| {
                TraderError::failed_to_apply_proxy(TraderBackend::RobinHood, proxy, err)
            })?)
        } else {
            client_builder
        }
        .build()
        .map_err(|err| {
            TraderError::failed_to_build_request_client(TraderBackend::RobinHood, err)
        })?;

        let mut headers = HeaderMap::new();
        headers.insert(ACCEPT, "*/*".parse().unwrap());
        headers.insert(ACCEPT_ENCODING, "gzip, deflate".parse().unwrap());
        headers.insert(
            ACCEPT_LANGUAGE,
            "en;q=1, fr;q=0.9, de;q=0.8, ja;q=0.7, nl;q=0.6, it;q=0.5"
                .parse()
                .unwrap(),
        );
        headers.insert(
            CONTENT_TYPE,
            "application/x-www-form-urlencoded; charset=utf-8"
                .parse()
                .unwrap(),
        );
        headers.insert("X-Robinhood-API-Version", "1.0.0".parse().unwrap());
        headers.insert(CONNECTION, "keep-alive".parse().unwrap());
        headers.insert(
            USER_AGENT,
            "Robinhood/823 (iPhone; iOS 7.1.2; Scale/2.00)"
                .parse()
                .unwrap(),
        );

        Ok(Self {
            settings,
            client,
            headers,
            auth_token: String::new(),
            refresh_token: String::new(),
            client_id: "c82SH0WZOsabOXGP2sxqcj34FxkvfnWRZBKlBjFS".to_string(),
        })
    }
}

impl TraderTrait for RobinhoodTrader {
    fn login(&mut self) -> Result<()> {
        let payload = RHLoginPayload::new(self.get_settings(), &self.client_id)?;
        let response = self
            .client
            .post(format!("{}{}", Self::BASE_URL, Self::LOGIN))
            .headers(self.headers.clone())
            .form(&payload)
            .send()
            .unwrap();

        let json = response.json::<RHLoginResponse>().unwrap();
        self.auth_token = json.access_token;
        self.refresh_token = json.refresh_token;
        self.headers
            .insert(AUTHORIZATION, self.auth_token.parse().unwrap());
        Ok(())
    }

    fn check_if_logged_in(&self, action: &str) -> Result<()> {
        if !self.headers.contains_key(AUTHORIZATION) {
            Err(TraderError::failed_to_complete_action_not_logged_in(
                self.get_trader_backend(),
                action,
            ))
        } else {
            Ok(())
        }
    }

    fn logout(&mut self) -> Result<()> {
        self.check_if_logged_in("logout")?;
        let payload = RHLogoutPayload::new(&self.client_id, &self.refresh_token);

        self.client
            .post(format!("{}{}", Self::BASE_URL, Self::LOGOUT))
            .headers(self.headers.clone())
            .form(&payload)
            .send()
            .unwrap();

        self.headers.remove(AUTHORIZATION);
        Ok(())
    }

    fn balance(&self, _ticker: &str) -> Result<f64> {
        self.check_if_logged_in("balance")?;
        Ok(0.0)
    }

    fn deposit(&self) -> Result<()> {
        self.check_if_logged_in("deposit")?;
        Ok(())
    }

    fn withdraw(&self) -> Result<()> {
        self.check_if_logged_in("withdraw")?;
        Ok(())
    }

    fn buy(&self, _ticker: &str, _amount: f64) -> Result<()> {
        self.check_if_logged_in("buy")?;
        Ok(())
    }

    fn sell(&self, _ticker: &str, _amount: f64) -> Result<()> {
        self.check_if_logged_in("sell")?;
        Ok(())
    }

    fn buy_crypto(&self, _ticker: &str, _amount: f64) -> Result<()> {
        self.check_if_logged_in("buy_crypto")?;
        Ok(())
    }

    fn sell_crypto(&self, _ticker: &str, _amount: f64) -> Result<()> {
        self.check_if_logged_in("sell_crypto")?;
        Ok(())
    }

    fn get_trader_backend(&self) -> TraderBackend {
        TraderBackend::RobinHood
    }

    fn get_settings(&self) -> &TraderSettings {
        &self.settings
    }

    fn set_settings(&mut self, settings: TraderSettings) {
        self.settings = settings
    }
}
