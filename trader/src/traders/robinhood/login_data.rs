use std::time::{SystemTime, UNIX_EPOCH};

use hmac::{Hmac, Mac};
use rand::{thread_rng, Rng};
use rebisu_configs::TraderSettings;
use rebisu_errors::Result;
use serde::{Deserialize, Serialize};
use sha1::Sha1;

type HmacSha1 = Hmac<Sha1>;

#[derive(Debug, Serialize)]
pub struct RHLoginPayload {
    pub username:     String,
    pub password:     String,
    pub grant_type:   String,
    pub client_id:    String,
    pub scope:        String,
    pub device_token: String,
    pub mfa_code:     String,
}

impl RHLoginPayload {
    fn generate_device_token() -> String {
        let mut rng = thread_rng();
        let mut rands = Vec::new();

        for i in 0..16 {
            let r = 4294967296.0 * rng.gen::<f64>();

            rands.push(((r as usize) >> ((3 & i) << 3)) & 255);
        }

        let mut hexa = Vec::new();
        for i in 0..256 {
            let hex_str = format!("{:x}", (i + 256));
            let lslice = hex_str[1..].to_string();
            hexa.push(lslice);
        }

        let mut id = String::new();
        for i in 0..16 {
            id.push_str(&hexa[rands[i]]);

            match i {
                3 | 5 | 7 | 9 => {
                    id.push('-');
                }
                _ => {}
            }
        }

        id
    }

    fn generate_mfa_token(secret: &str) -> Result<String> {
        // TODO @gluax clean up unwraps
        let seconds_since_epoch = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_secs();
        let key = base32::decode(base32::Alphabet::RFC4648 { padding: false }, secret).unwrap();
        let msg = (seconds_since_epoch / 30).to_be_bytes();

        let mut mac = HmacSha1::new_from_slice(&key).unwrap();
        mac.update(&msg);
        let result = mac.finalize();

        let h = result.into_bytes();
        let o = (h[19] & 15) as usize;
        let sub: [u8; 4] = [h[o], h[o + 1], h[o + 2], h[o + 3]];
        let unpacked = u32::from_be_bytes(sub);

        Ok(format!("{:06}", (unpacked & 0x7fffffff) % 1_000_000))
    }

    pub fn new(settings: &TraderSettings, id: &str) -> Result<Self> {
        Ok(Self {
            username:     settings.username.clone(),
            password:     settings.password.clone(),
            grant_type:   "password".to_string(),
            client_id:    id.to_string(),
            scope:        "internal".to_string(),
            device_token: Self::generate_device_token(),
            mfa_code:     Self::generate_mfa_token(settings.qr_code.as_ref().unwrap())?,
        })
    }
}

#[derive(Debug, Deserialize)]
pub struct RHLoginResponse {
    pub access_token:  String,
    pub expires_in:    u64,
    pub token_type:    String,
    pub scope:         String,
    pub refresh_token: String,
    pub mfa_code:      String,
    pub backup_code:   Option<String>,
}
