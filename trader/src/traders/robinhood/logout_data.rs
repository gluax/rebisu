use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct RHLogoutPayload {
    pub client_id: String,
    pub token:     String,
}

impl RHLogoutPayload {
    pub fn new(id: &str, token: &str) -> Self {
        Self {
            client_id: id.to_string(),
            token:     token.to_string(),
        }
    }
}
