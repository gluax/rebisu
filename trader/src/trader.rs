use bevy::prelude::Component;
use rebisu_common::TraderBackend;
use rebisu_configs::TraderSettings;
use rebisu_errors::{AppError, ErrorHandler, Result};

use super::{NullTrader, RobinhoodTrader, TraderTrait};

#[derive(Component)]
pub struct Trader {
    inner: Box<dyn TraderTrait>,
}

impl Trader {
    pub fn new(
        trader: &TraderBackend,
        settings: TraderSettings,
        handler: &mut ErrorHandler,
    ) -> Result<Self> {
        let inner: Box<dyn TraderTrait> = match trader {
            TraderBackend::RobinHood => Box::new(RobinhoodTrader::new(settings)?),
            TraderBackend::NullBackend(backend) => {
                handler.emit_err(AppError::invalid_trader_backend(&backend));

                Box::new(NullTrader::new(settings, backend.clone()))
            }
        };

        Ok(Self { inner })
    }
}

impl TraderTrait for Trader {
    fn login(&mut self) -> Result<()> {
        self.inner.login()
    }

    fn check_if_logged_in(&self, action: &str) -> Result<()> {
        self.inner.check_if_logged_in(action)
    }

    fn logout(&mut self) -> Result<()> {
        self.inner.logout()
    }

    fn balance(&self, ticker: &str) -> Result<f64> {
        self.inner.balance(ticker)
    }

    fn deposit(&self) -> Result<()> {
        self.inner.deposit()
    }

    fn withdraw(&self) -> Result<()> {
        self.inner.withdraw()
    }

    fn buy(&self, ticker: &str, amount: f64) -> Result<()> {
        self.inner.buy(ticker, amount)
    }

    fn sell(&self, ticker: &str, amount: f64) -> Result<()> {
        self.inner.sell(ticker, amount)
    }

    fn buy_crypto(&self, ticker: &str, amount: f64) -> Result<()> {
        self.inner.buy_crypto(ticker, amount)
    }

    fn sell_crypto(&self, ticker: &str, amount: f64) -> Result<()> {
        self.inner.sell_crypto(ticker, amount)
    }

    fn get_trader_backend(&self) -> TraderBackend {
        self.inner.get_trader_backend()
    }

    fn get_settings(&self) -> &TraderSettings {
        self.inner.get_settings()
    }

    fn set_settings(&mut self, settings: TraderSettings) {
        self.inner.set_settings(settings)
    }
}
