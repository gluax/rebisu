use rebisu_common::TraderBackend;
use rebisu_configs::TraderSettings;
use rebisu_errors::Result;

pub trait TraderTrait: Send + Sync + 'static {
    fn login(&mut self) -> Result<()>;
    fn check_if_logged_in(&self, action: &str) -> Result<()>;
    fn logout(&mut self) -> Result<()>;

    fn balance(&self, ticker: &str) -> Result<f64>;

    fn deposit(&self) -> Result<()>;
    fn withdraw(&self) -> Result<()>;

    fn buy(&self, ticker: &str, amount: f64) -> Result<()>;
    fn sell(&self, ticker: &str, amount: f64) -> Result<()>;

    fn buy_crypto(&self, ticker: &str, amount: f64) -> Result<()>;
    fn sell_crypto(&self, ticker: &str, amount: f64) -> Result<()>;

    fn get_trader_backend(&self) -> TraderBackend;

    fn get_settings(&self) -> &TraderSettings;
    fn set_settings(&mut self, settings: TraderSettings);
}
