use rebisu_common::TraderBackend;
use rebisu_configs::TraderSettings;
use rebisu_errors::Result;

use super::TraderTrait;

#[derive(Clone, Debug, Default)]
pub struct NullTrader {
    settings: TraderSettings,
    name:     String,
}

impl NullTrader {
    pub fn new(settings: TraderSettings, name: String) -> Self {
        Self { settings, name }
    }
}

impl TraderTrait for NullTrader {
    fn login(&mut self) -> Result<()> {
        Ok(())
    }

    fn check_if_logged_in(&self, _action: &str) -> Result<()> {
        Ok(())
    }

    fn logout(&mut self) -> Result<()> {
        Ok(())
    }

    fn balance(&self, _ticker: &str) -> Result<f64> {
        Ok(0.0)
    }

    fn deposit(&self) -> Result<()> {
        Ok(())
    }

    fn withdraw(&self) -> Result<()> {
        Ok(())
    }

    fn buy(&self, _ticker: &str, _amount: f64) -> Result<()> {
        Ok(())
    }

    fn sell(&self, _ticker: &str, _amount: f64) -> Result<()> {
        Ok(())
    }

    fn buy_crypto(&self, _ticker: &str, _amount: f64) -> Result<()> {
        Ok(())
    }

    fn sell_crypto(&self, _ticker: &str, _amount: f64) -> Result<()> {
        Ok(())
    }

    fn get_trader_backend(&self) -> TraderBackend {
        TraderBackend::NullBackend(self.name.clone())
    }

    fn get_settings(&self) -> &TraderSettings {
        &self.settings
    }

    fn set_settings(&mut self, settings: TraderSettings) {
        self.settings = settings
    }
}
