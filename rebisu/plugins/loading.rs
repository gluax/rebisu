use bevy::{
    app::{App, Plugin},
    prelude::{IntoChainSystem, IntoSystem, StartupStage, SystemSet},
    utils::HashMap,
};
#[cfg(debug_assertions)]
use bevy::{
    diagnostic::{DiagnosticsPlugin, FrameTimeDiagnosticsPlugin, LogDiagnosticsPlugin},
    log::LogPlugin,
};
use clap::StructOpt;
use rebisu_logger::TradeLog;

use super::{TraderHeadlessPlugin, TraderPlugin};
use crate::{
    cli::{Rebisu, RebisuCommands},
    common::Tickers,
    events::TickerUpdate,
    systems::*,
};

pub struct LoadingPlugin;

impl Plugin for LoadingPlugin {
    fn build(&self, app: &mut App) {
        let rebisu_options = Rebisu::parse();

        app.insert_resource(rebisu_options.encryption_key);
        app.add_startup_system_set_to_stage(
            StartupStage::PreStartup,
            SystemSet::new().with_system(init_configs_dir_system.chain(error_handler_system)),
        );

        match rebisu_options.command {
            RebisuCommands::Run(run_options) => {
                let known_tickers: Tickers = HashMap::default();

                app.add_event::<TradeLog>()
                    .add_event::<TickerUpdate>()
                    .insert_resource(run_options.singleton)
                    .insert_resource(known_tickers);

                app.add_startup_system_set_to_stage(
                    StartupStage::Startup,
                    SystemSet::new()
                        .with_system(load_traders_configs_system.chain(error_handler_system))
                        .with_system(load_ticker_configs_system.chain(error_handler_system)),
                );

                if run_options.headless {
                    app.add_plugin(TraderHeadlessPlugin);
                } else {
                    app.add_plugin(TraderPlugin);
                }
            }
            RebisuCommands::InitTrader(init_trader) => {
                app.insert_resource(init_trader);

                app.add_startup_system_set_to_stage(
                    StartupStage::Startup,
                    SystemSet::new()
                        .with_system(create_trader_config_system.chain(error_handler_system)),
                );

                app.add_system(exit_system.system());
            }
        }

        #[cfg(debug_assertions)]
        {
            app.add_plugin(LogPlugin::default())
                .add_plugin(DiagnosticsPlugin::default())
                .add_plugin(FrameTimeDiagnosticsPlugin::default())
                .add_plugin(LogDiagnosticsPlugin::default());
        }
    }
}
