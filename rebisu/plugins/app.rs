use bevy::{app::App, prelude::Plugin};
use rebisu_errors::ErrorHandler;

use super::{LoadingPlugin, RebisuState};

pub struct RebisuPlugin;

impl Plugin for RebisuPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<ErrorHandler>()
            .add_state(RebisuState::Loading)
            .add_plugin(LoadingPlugin);
    }
}
