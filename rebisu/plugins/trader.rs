use bevy::{
    app::App,
    prelude::{IntoSystem, Plugin, SystemSet},
};

use super::RebisuState;
use crate::systems::trader_system;

pub struct TraderPlugin;

impl Plugin for TraderPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(
            SystemSet::on_update(RebisuState::Running).with_system(trader_system.system()),
        );
    }
}
