use bevy::{
    app::App,
    prelude::{IntoChainSystem, IntoSystem, Plugin, SystemSet},
};

use super::RebisuState;
use crate::systems::*;

pub struct TraderHeadlessPlugin;

impl Plugin for TraderHeadlessPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(
            SystemSet::on_update(RebisuState::Running)
                .with_system(trader_log_in_system.system())
                .with_system(trader_system.system())
                .with_system(csv_logger_system.chain(error_handler_system))
                .with_system(ticker_watcher_system.chain(error_handler_system))
                .with_system(update_ticker_system.system()),
        );
    }
}
