// This example game uses States to separate logic
// See https://bevy-cheatbook.github.io/programming/states.html
// Or https://github.com/bevyengine/bevy/blob/main/examples/ecs/state.rs
#[derive(Clone, Eq, PartialEq, Debug, Hash)]
pub enum RebisuState {
    /// During the loading State the LoadingPlugin will load the existing
    /// configurations.
    Loading,
    /// During the running State either the TraderHeadlessPlugin or TraderPlugin
    /// will run our application logic depending on user desire.
    Running,
}
