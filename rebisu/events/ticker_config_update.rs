use rebisu_configs::TickerSettings;

pub struct TickerUpdate {
    pub ticker_name: &'static str,
    pub settings:    TickerSettings,
    pub interval:    f32,
}
