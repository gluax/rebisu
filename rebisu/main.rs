#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

mod bundles;
mod cli;
mod common;
mod components;
mod events;
mod plugins;
mod systems;

use bevy::{
    app::{App, ScheduleRunnerSettings},
    utils::Duration,
    MinimalPlugins,
};

use crate::plugins::RebisuPlugin;

fn main() {
    let mut app = App::new();

    app.insert_resource(ScheduleRunnerSettings::run_loop(Duration::from_secs_f64(
        1.0,
    )))
    .add_plugins(MinimalPlugins)
    .add_plugin(RebisuPlugin);

    app.run();
}
