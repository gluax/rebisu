use bevy::prelude::Component;

#[derive(Component, Debug)]
pub struct Name<'a>(pub &'a str);
