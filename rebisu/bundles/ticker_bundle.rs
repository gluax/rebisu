use bevy::{core::Timer, prelude::Bundle};
use rebisu_ticker::Ticker;

#[derive(Bundle)]
pub struct TickerBundle {
    pub ticker: Ticker,
    pub timer:  Timer,
}

impl TickerBundle {
    pub fn new(ticker: Ticker, interval: f32) -> Self {
        Self {
            ticker,
            timer: Timer::from_seconds(interval, true),
        }
    }
}
