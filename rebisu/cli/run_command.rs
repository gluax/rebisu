use clap::Args;

#[derive(Args, Debug)]
pub struct RunCommand {
    /// Run in headless mode spawning a web app?(todo) if true.
    /// Otherwise runs a GUI app(todo).
    /// TODO make this a subcommand with singleton as parameter of headless
    /// mode. Reason being a GUI would have a run once button.
    #[clap(long)]
    pub headless:  bool,
    /// Run all tickers and scripts once.
    #[clap(short, long)]
    pub singleton: bool,
}
