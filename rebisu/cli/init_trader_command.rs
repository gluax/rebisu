use clap::Args;

#[derive(Args, Clone, Debug)]
pub struct InitTraderCommand {
    #[clap(short, long)]
    pub trader_name: String,
    #[clap(short, long)]
    pub username:    String,
    #[clap(short, long)]
    pub password:    String,
    #[clap(short, long)]
    pub qr_code:     Option<String>,
}
