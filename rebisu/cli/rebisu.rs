use clap::{Parser, Subcommand};

use super::{InitTraderCommand, RunCommand};

#[derive(Debug, Parser)]
#[clap(name = "rebisu", version = "0.1.0")]
pub struct Rebisu {
    #[clap(short, long)]
    pub encryption_key: String,
    #[clap(subcommand)]
    pub command:        RebisuCommands,
}

#[derive(Debug, Subcommand)]
pub enum RebisuCommands {
    InitTrader(InitTraderCommand),
    Run(RunCommand),
}
