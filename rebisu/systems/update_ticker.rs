use std::time::Duration;

use bevy::{
    core::Timer,
    prelude::{EventReader, Query},
};
use rebisu_ticker::{Ticker, TickerTrait};

use crate::events::TickerUpdate;

pub fn update_ticker_system(
    mut ticker_updates: EventReader<TickerUpdate>,
    mut tickers: Query<(&mut Ticker, &mut Timer)>,
) {
    for updated_ticker in ticker_updates.iter() {
        for (mut ticker, mut timer) in tickers.iter_mut() {
            if updated_ticker.ticker_name == ticker.get_ticker_name() {
                ticker.set_settings(updated_ticker.settings.clone());
                timer.set_duration(Duration::from_secs_f32(updated_ticker.interval));
            }
        }
    }
}
