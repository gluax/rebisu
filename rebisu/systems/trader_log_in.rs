use std::sync::Arc;

use bevy::{
    prelude::{Query, Res, ResMut},
    tasks::ComputeTaskPool,
};
use parking_lot::Mutex;
use rebisu_errors::{AppError, ErrorHandler};
use rebisu_trader::{Trader, TraderTrait};

pub fn trader_log_in_system(
    pool: Res<ComputeTaskPool>,
    mut handler: ResMut<ErrorHandler>,
    mut traders: Query<&mut Trader>,
) {
    let err_collector = Arc::new(Mutex::new(Vec::new()));

    traders.par_for_each_mut(&pool, 1, |mut trader| {
        // if we aren't logged in just log in
        if trader.check_if_logged_in("log_in_check").is_err() {
            if let Err(err) = trader.login() {
                err_collector.lock().push(err);
            }
        }
    });

    let errors = Arc::try_unwrap(err_collector).map_err(AppError::failed_to_read_trader_errors);

    match errors {
        Ok(errors) => {
            for err in errors.into_inner().into_iter() {
                handler.emit_err(err);
            }
        }
        Err(err) => handler.emit_err(err),
    }
}
