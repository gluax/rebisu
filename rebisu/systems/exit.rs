use bevy::{app::AppExit, prelude::EventWriter};

pub fn exit_system(mut exit: EventWriter<AppExit>) {
    exit.send(AppExit);
}
