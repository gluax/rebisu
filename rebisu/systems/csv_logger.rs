use bevy::prelude::{EventReader, ResMut};
use rebisu_errors::Result;
use rebisu_logger::{Logger, TradeLog, TradeLogger};

pub fn csv_logger_system(
    mut logger: ResMut<TradeLogger>,
    mut trade_logs: EventReader<TradeLog>,
) -> Result<()> {
    for trade_log in trade_logs.iter() {
        logger.log(trade_log)?;
    }

    Ok(())
}
