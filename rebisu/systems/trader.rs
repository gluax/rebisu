use std::{
    path::{Path, PathBuf},
    sync::Arc,
};

use bevy::{
    core::{Time, Timer},
    prelude::{EventWriter, Query, Res, ResMut},
    tasks::ComputeTaskPool,
    utils::Instant,
};
use notify_rust::Notification;
use parking_lot::Mutex;
use rebisu_common::ScriptResult;
use rebisu_configs::TickerSettings;
use rebisu_errors::{AppError, ErrorHandler, Result};
use rebisu_logger::TradeLog;
use rebisu_scriptor::{Script, Scriptor};
use rebisu_ticker::{Ticker, TickerTrait};

fn buy_sell_neither(result: &ScriptResult) -> Result<()> {
    match result {
        ScriptResult::Buy(amt) => Notification::new()
            .summary("Rebisu")
            .body(&format!("BTC Buy Time amt: {}", amt))
            .show()
            .map_err(AppError::failed_to_create_desktop_notification),
        ScriptResult::Sell(amt) => Notification::new()
            .summary("Rebisu")
            .body(&format!("BTC Sell Time amt: {}", amt))
            .show()
            .map_err(AppError::failed_to_create_desktop_notification),
        ScriptResult::Neither => Ok(()),
    }
}

fn run_script(
    ticker: &Ticker,
    settings: &TickerSettings,
    rebisu_path: &Path,
) -> Result<(ScriptResult, f64, f64)> {
    let mut now = Instant::now();
    let data = ticker.download(None, None)?;
    let data_download_time = now.elapsed().as_secs_f64();

    if data.is_empty() {
        return Err(AppError::data_fetch_error(
            ticker.get_ticker_name(),
            ticker.get_ticker_backend(),
        ));
    }

    let script_path = rebisu_path.join("scripts").join(&settings.buy_sell_script);
    now = Instant::now();
    let script = Scriptor::load(&script_path)?;
    let script_run_time = now.elapsed().as_secs_f64();

    let script_result = script.run(data)?;

    Ok((script_result, data_download_time, script_run_time))
}

fn trade(ticker: &Ticker, settings: &TickerSettings, rebisu_path: &Path) -> Result<TradeLog> {
    let (script_result, data_download_time, script_run_time) =
        run_script(ticker, settings, rebisu_path)?;
    buy_sell_neither(&script_result)?;

    let trade_log = TradeLog::new(
        ticker,
        settings.ticker_backend,
        settings.trader_backend.clone(),
        script_result,
        data_download_time,
        script_run_time,
    );
    Ok(trade_log)
}

pub fn trader_system(
    pool: Res<ComputeTaskPool>,
    time: Res<Time>,
    app_path: Res<PathBuf>,
    mut trade_logs: EventWriter<TradeLog>,
    mut handler: ResMut<ErrorHandler>,
    mut tickers: Query<(&Ticker, &mut Timer)>,
) {
    let err_collector = Arc::new(Mutex::new(Vec::new()));
    let log_collector = Arc::new(Mutex::new(Vec::new()));

    tickers.par_for_each_mut(&pool, 1, |(ticker, mut timer)| {
        timer.tick(time.delta());

        let settings = ticker.get_settings();

        if timer.finished() {
            match trade(ticker, settings, &app_path) {
                Ok(trade_log) => log_collector.lock().push(trade_log),
                Err(err) => err_collector.lock().push(err),
            }
        }
    });

    // TODO: Handle unwrap
    let logs = Arc::try_unwrap(log_collector).unwrap();

    for trade_log in logs.into_inner().into_iter() {
        trade_logs.send(trade_log);
    }

    let errors = Arc::try_unwrap(err_collector).map_err(AppError::failed_to_read_trader_errors);

    match errors {
        Ok(errors) => {
            for err in errors.into_inner().into_iter() {
                handler.emit_err(err);
            }
        }
        Err(err) => handler.emit_err(err),
    }
}
