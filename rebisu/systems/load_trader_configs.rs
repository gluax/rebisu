use std::{fs, path::PathBuf};

use bevy::prelude::{Commands, Res, ResMut};
use rebisu_common::TraderBackend;
use rebisu_configs::TraderSettings;
use rebisu_errors::{AppError, ErrorHandler, Result};
use rebisu_trader::Trader;

pub fn load_traders_configs_system(
    mut commands: Commands,
    mut handler: ResMut<ErrorHandler>,
    rebisu_configs: Res<PathBuf>,
    encryption_key: Res<String>,
) -> Result<()> {
    let trader_configs = rebisu_configs.join("traders");

    for file in fs::read_dir(trader_configs).map_err(AppError::failed_to_read_settings_directory)? {
        let file = file.map_err(AppError::invalid_file_found_in_settings_directory)?;
        let file_path = file.path();

        let trader_name = file_path
            .file_stem()
            .ok_or_else(|| AppError::file_stem_get_error(file.path()))?
            .to_str()
            .ok_or_else(|| AppError::failed_to_convert_os_string(file.path()))?;

        let trader_backend = TraderBackend::from(trader_name.to_string());
        let trader_settings = TraderSettings::load(&file_path, &trader_backend, &encryption_key)?;
        let trader = Trader::new(&trader_backend, trader_settings, &mut handler)?;

        let entity = commands.spawn().id();
        commands.entity(entity).insert(trader);
    }

    Ok(())
}
