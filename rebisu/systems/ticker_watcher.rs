use std::{path::PathBuf, time::SystemTime};

use bevy::{
    prelude::{Commands, EventWriter, Res, ResMut},
    utils::HashMap,
};
use rebisu_errors::{ErrorHandler, Result};

use crate::{common::iterate_ticker_configs, events::TickerUpdate};

pub fn ticker_watcher_system(
    mut commands: Commands,
    mut handler: ResMut<ErrorHandler>,
    mut tickers: ResMut<HashMap<&'static str, SystemTime>>,
    ticker_events: EventWriter<TickerUpdate>,
    app_path: Res<PathBuf>,
) -> Result<()> {
    iterate_ticker_configs(
        &mut commands,
        &mut handler,
        &mut tickers,
        ticker_events,
        &app_path,
    )
}
