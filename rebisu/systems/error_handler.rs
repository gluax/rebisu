use bevy::prelude::{In, ResMut};
use rebisu_errors::{ErrorHandler, Result};

pub fn error_handler_system(In(result): In<Result<()>>, mut handler: ResMut<ErrorHandler>) {
    if let Err(err) = result {
        handler.emit_err(err);
    }
}
