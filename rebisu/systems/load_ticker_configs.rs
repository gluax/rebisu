use std::path::PathBuf;

use bevy::prelude::{Commands, EventWriter, Res, ResMut, State};
use rebisu_errors::{AppError, ErrorHandler, Result};
use rebisu_logger::TradeLogger;

use crate::{
    common::{iterate_ticker_configs, Tickers},
    events::TickerUpdate,
    plugins::RebisuState,
};

pub fn load_ticker_configs_system(
    mut commands: Commands,
    mut handler: ResMut<ErrorHandler>,
    mut tickers: ResMut<Tickers>,
    ticker_events: EventWriter<TickerUpdate>,
    mut app_state: ResMut<State<RebisuState>>,
    rebisu_configs: Res<PathBuf>,
) -> Result<()> {
    let logs_dir = rebisu_configs.join("logs");
    let trades_log = logs_dir.join("trades.csv");
    let trade_logger = TradeLogger::new(&trades_log)?;
    commands.insert_resource(trade_logger);

    iterate_ticker_configs(
        &mut commands,
        &mut handler,
        &mut tickers,
        ticker_events,
        &rebisu_configs,
    )?;

    app_state
        .set(RebisuState::Running)
        .map_err(AppError::failed_to_exit_load_state)
}
