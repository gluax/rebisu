use std::fs;

use bevy::prelude::Commands;
use directories::ProjectDirs;
use rebisu_configs::TickerSettings;
use rebisu_errors::{AppError, Result};

pub fn init_configs_dir_system(mut commands: Commands) -> Result<()> {
    if let Some(ref project_dirs) = ProjectDirs::from_path("rebisu".into()) {
        let rebisu_configs = project_dirs.config_dir();
        commands.insert_resource(rebisu_configs.to_path_buf());

        let scripts_dir = rebisu_configs.join("scripts");
        if !scripts_dir.exists() {
            // TODO: Clean up unwrap.
            fs::create_dir(scripts_dir).unwrap();
        }

        let logs_dir = rebisu_configs.join("logs");
        if !logs_dir.exists() {
            // TODO: Clean up unwrap.
            fs::create_dir(&logs_dir).unwrap();
        }

        let tickers_dir = rebisu_configs.join("tickers");
        if !tickers_dir.exists() || !tickers_dir.join("_template.toml").exists() {
            TickerSettings::create_template(&tickers_dir)?;
        }

        let traders_dir = rebisu_configs.join("traders");
        if !traders_dir.exists() {
            fs::create_dir(&traders_dir).unwrap();
        }

        Ok(())
    } else {
        Err(AppError::failed_to_find_settings_directory())
    }
}
