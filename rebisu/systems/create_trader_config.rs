use std::path::PathBuf;

use bevy::prelude::Res;
use rebisu_common::TraderBackend;
use rebisu_configs::TraderSettings;
use rebisu_errors::Result;

use crate::cli::InitTraderCommand;

pub fn create_trader_config_system(
    init_trader_options: Res<InitTraderCommand>,
    rebisu_configs: Res<PathBuf>,
    encryption_key: Res<String>,
) -> Result<()> {
    let trader = TraderBackend::from(init_trader_options.trader_name.clone());

    let traders_dir = rebisu_configs.join("traders");
    let trader_settings = traders_dir.join(&format!("{}.toml", trader));
    TraderSettings::write(
        &trader_settings,
        trader,
        &encryption_key,
        &init_trader_options.username,
        &init_trader_options.password,
        init_trader_options.qr_code.as_ref(),
    )
}
