use std::{fs, path::Path};

use bevy::prelude::{Commands, EventWriter};
use rebisu_errors::{AppError, ErrorHandler, Result};

use super::{load_ticker_from_settings, Tickers};
use crate::events::TickerUpdate;

pub fn iterate_ticker_configs(
    commands: &mut Commands,
    handler: &mut ErrorHandler,
    tickers: &mut Tickers,
    mut ticker_events: EventWriter<TickerUpdate>,
    rebisu_configs: &Path,
) -> Result<()> {
    let ticker_configs = rebisu_configs.join("tickers");

    for file in fs::read_dir(ticker_configs).map_err(AppError::failed_to_read_settings_directory)? {
        let file = file.map_err(AppError::invalid_file_found_in_settings_directory)?;
        let file_path = file.path();

        let ticker_name = file_path
            .file_stem()
            .ok_or_else(|| AppError::file_stem_get_error(file.path()))?
            .to_str()
            .ok_or_else(|| AppError::failed_to_convert_os_string(file.path()))?;
        let boxed = Box::new(ticker_name.to_string());
        if ticker_name != "_template" {
            load_ticker_from_settings(
                commands,
                handler,
                tickers,
                &mut ticker_events,
                rebisu_configs,
                Box::leak(boxed),
            )?;
        }
    }

    Ok(())
}
