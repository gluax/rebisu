use std::{fs, path::Path, time::SystemTime};

use bevy::{
    prelude::{Commands, EventWriter},
    utils::HashMap,
};
use rebisu_configs::TickerSettings;
use rebisu_errors::{ErrorHandler, Result};
use rebisu_ticker::Ticker;

use crate::{bundles::TickerBundle, events::TickerUpdate};

// Should this function be separated into load_existing ticker
// and load new ticker?
pub fn load_ticker_from_settings(
    commands: &mut Commands,
    handler: &mut ErrorHandler,
    tickers: &mut HashMap<&'static str, SystemTime>,
    ticker_events: &mut EventWriter<TickerUpdate>,
    rebisu_folder_path: &Path,
    ticker_name: &'static str,
) -> Result<()> {
    let settings_path = &(*rebisu_folder_path).join(format!("tickers/{}.toml", ticker_name));
    let settings = TickerSettings::load(settings_path)?;
    let interval = settings.run_interval.as_f32();

    // TODO figure out how to generalize for future config files.
    // Maybe by Passing Hashmap as generic?
    // TODO handle these unwraps.
    let metadata = fs::metadata(settings_path).unwrap();
    let modified = metadata.modified().unwrap();
    if let Some(last_modified) = tickers.get(ticker_name) {
        // This case is to check if the ticker settings updated.
        if modified == *last_modified {
            // If it did just exit the function.
            return Ok(());
        } else {
            // Otherwise create a bevy event to mark it for updating.
            ticker_events.send(TickerUpdate {
                ticker_name,
                settings,
                interval,
            });

            // Make sure to update with new modified time now.
            tickers.insert(ticker_name, modified);
        }
    } else {
        // this case is to add ticker if does not exist.
        tickers.insert(ticker_name, modified);

        let ticker: Ticker = Ticker::new(ticker_name, settings, handler)?;
        let entity = commands.spawn().id();
        commands
            .entity(entity)
            .insert_bundle(TickerBundle::new(ticker, interval));
    }

    Ok(())
}
