use std::time::SystemTime;

use bevy::utils::HashMap;

pub type Tickers = HashMap<&'static str, SystemTime>;
