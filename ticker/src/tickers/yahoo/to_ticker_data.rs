use std::convert::TryFrom;

use rebisu_errors::{RebisuError, Result, TickerError};

use super::YTHRCR;
use crate::TickerData;

impl TryFrom<YTHRCR> for TickerData {
    type Error = RebisuError;

    fn try_from(history_chart_response: YTHRCR) -> Result<Self> {
        let ticker_name = history_chart_response
            .ticker_name
            .unwrap_or_else(|| "unknown".to_string());

        let result = history_chart_response
            .chart
            .result
            .get(0)
            .ok_or_else(|| TickerError::no_result_data("Yahoo", &ticker_name))?;

        let data = result.data(&ticker_name)?;

        Ok(Self {
            dates:  result.timestamp.clone(),
            open:   data.open.clone(),
            high:   data.high.clone(),
            low:    data.low.clone(),
            close:  data.close.clone(),
            volume: data.volume.clone(),
        })
    }
}
