use chrono::{serde::ts_seconds, DateTime, Utc};
use serde::{Deserialize, Serialize};

pub fn serialize<S>(dts: &[DateTime<Utc>], serializer: S) -> Result<S::Ok, S::Error>
where
    S: serde::ser::Serializer,
{
    dts.iter()
        .map(|dt| dt.timestamp())
        .collect::<Vec<i64>>()
        .serialize(serializer)
}

pub fn deserialize<'de, D>(deserializer: D) -> Result<Vec<DateTime<Utc>>, D::Error>
where
    D: serde::de::Deserializer<'de>,
{
    #[derive(Debug, Deserialize)]
    struct Wrapper(#[serde(with = "ts_seconds")] DateTime<Utc>);

    let v = Vec::deserialize(deserializer)?;

    Ok(v.into_iter().map(|Wrapper(dt)| dt).collect())
}
