use std::{convert::TryFrom, fmt};

use rebisu_common::{Interval, Period, TickerBackend};
use rebisu_configs::TickerSettings;
use rebisu_errors::{Result, TickerError};
use reqwest::{blocking::Client, header::USER_AGENT, Proxy};

use super::YTHRCR;
use crate::{TickerData, TickerTrait};

#[derive(Clone, Debug)]
pub struct YFinance {
    ticker_name: &'static str,
    settings:    TickerSettings,
    client:      Client,
}

impl YFinance {
    const API_VERSION: &'static str = "v8";
    const BASE_URL: &'static str = "https://query2.finance.yahoo.com";
    const FINANCE_CHART_EP: &'static str = "finance/chart";

    pub fn new(ticker_name: &'static str, settings: TickerSettings) -> Result<Self> {
        let client_builder = Client::builder();
        let client = if let Some(proxy) = &settings.proxy {
            client_builder.proxy(Proxy::https(proxy).map_err(|err| {
                TickerError::failed_to_apply_proxy(TickerBackend::Yahoo, &ticker_name, proxy, err)
            })?)
        } else {
            client_builder
        }
        .build()
        .map_err(|err| {
            TickerError::failed_to_build_request_client(TickerBackend::Yahoo, &ticker_name, err)
        })?;

        Ok(Self {
            ticker_name,
            settings,
            client,
        })
    }
}

impl TickerTrait for YFinance {
    fn download(
        &self,
        auto_adjust_ohlc: Option<bool>,
        prepost: Option<bool>,
    ) -> Result<TickerData> {
        // TODO: clean up tickertrait for Yahoo.

        self.validate_interval()?;
        self.validate_period()?;

        let period = self.settings.data_period;
        let interval = self.settings.data_interval;
        let _auto_adjust_ohlc = auto_adjust_ohlc.unwrap_or(false);
        let prepost = prepost.unwrap_or(false);

        let endpoint = format!(
            "{}/{}/{}/{}",
            Self::BASE_URL,
            Self::API_VERSION,
            Self::FINANCE_CHART_EP,
            self.ticker_name
        );

        /* let data = client
            .get(&endpoint)
            .header(USER_AGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36")
            .query(&[("range", period.to_string()), ("interval", interval.to_string()), ("includePrePost", prepost.to_string()), ("events", "div,splits".to_string())])
            .send()
            .await
            .unwrap();

        let file = std::fs::File::create("test.json").unwrap();
        let writer = std::io::BufWriter::new(file);
        let txt = data.text().await.unwrap();
        let ah: serde_json::Value = serde_json::from_str(&txt).unwrap();
        serde_json::to_writer_pretty(writer, &ah).unwrap(); */

        let data = self.client
            .get(endpoint)
            .header(USER_AGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36")
            .query(&[("range", period.to_string()), ("interval", interval.to_string()), ("includePrePost", prepost.to_string()), ("events", "div,splits".to_string())])
            .send()
            .map_err(|err| TickerError::failed_to_fetch_ticker_data(self.get_ticker_backend(), &self.ticker_name, err))?;

        let mut json_data = data.json::<YTHRCR>().map_err(|err| {
            TickerError::failed_to_format_ticker_data(
                self.get_ticker_backend(),
                &self.ticker_name,
                err,
            )
        })?;

        json_data.set_name(self.ticker_name.to_string());

        TickerData::try_from(json_data)
    }

    fn get_ticker_name(&self) -> &'static str {
        self.ticker_name
    }

    fn get_ticker_backend(&self) -> TickerBackend {
        TickerBackend::Yahoo
    }

    fn get_settings(&self) -> &TickerSettings {
        &self.settings
    }

    fn set_settings(&mut self, settings: TickerSettings) {
        self.settings = settings;
    }

    fn validate_interval(&self) -> Result<()> {
        use Interval::*;

        match self.settings.data_interval {
            Minutes(1) => Ok(()),
            Minutes(2) => Ok(()),
            Minutes(5) => Ok(()),
            Minutes(15) => Ok(()),
            Minutes(30) => Ok(()),
            Minutes(60) => Ok(()),
            Minutes(90) => Ok(()),
            Days(1) => Ok(()),
            Days(5) => Ok(()),
            Weeks(1) => Ok(()),
            Months(1) => Ok(()),
            Months(3) => Ok(()),
            _ => Err(TickerError::invalid_ticker_interval(
                self.get_ticker_backend(),
                self.get_ticker_name(),
                self.settings.data_interval,
            )),
        }
    }

    fn validate_period(&self) -> Result<()> {
        use Period::*;

        match self.settings.data_period {
            Days(1) => Ok(()),
            Days(5) => Ok(()),
            Weeks(1) => Ok(()),
            Months(1) => Ok(()),
            Months(3) => Ok(()),
            Months(6) => Ok(()),
            Years(1) => Ok(()),
            Years(2) => Ok(()),
            Years(5) => Ok(()),
            Years(10) => Ok(()),
            Max => Ok(()),
            YTD => Ok(()),
            _ => Err(TickerError::invalid_ticker_period(
                self.get_ticker_backend(),
                self.get_ticker_name(),
                self.settings.data_period,
            )),
        }
    }
}

impl fmt::Display for YFinance {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.ticker_name)
    }
}
