use chrono::{serde::ts_seconds, DateTime, Utc};
use rebisu_errors::{Result, TickerError};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct YTHRCR {
    pub chart:       YTHRC,
    pub ticker_name: Option<String>,
}

impl YTHRCR {
    pub(crate) fn set_name(&mut self, name: String) {
        self.ticker_name = Some(name);
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct YTHRC {
    pub error:  Option<String>,
    pub result: Vec<YTHRCRes>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct YTHRCRes {
    pub indicators: YTHRCRI,
    pub meta:       YTHRCRM,
    #[serde(with = "super::datetime_vec")]
    pub timestamp:  Vec<DateTime<Utc>>,
}

impl YTHRCRes {
    pub fn data(&self, ticker_name: &str) -> Result<&YTHRCRIQ> {
        self.indicators
            .quote
            .get(0)
            .ok_or_else(|| TickerError::no_result_data("Yahoo", ticker_name))
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct YTHRCRI {
    pub quote: Vec<YTHRCRIQ>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct YTHRCRIQ {
    pub close:  Vec<Option<f64>>,
    pub high:   Vec<Option<f64>>,
    pub low:    Vec<Option<f64>>,
    pub open:   Vec<Option<f64>>,
    pub volume: Vec<Option<u64>>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct YTHRCRM {
    pub chart_previous_close:   f64,
    pub currency:               String,
    pub current_trading_period: YTHRCRMCTP,
    pub data_granularity:       String,
    pub exchange_name:          String,
    pub exchange_timezone_name: String,
    #[serde(with = "ts_seconds")]
    pub first_trade_date:       DateTime<Utc>,
    pub gmtoffset:              u32,
    pub instrument_type:        String,
    pub previous_close:         f64,
    pub price_hint:             u8,
    pub range:                  String,
    pub regular_market_price:   f64,
    #[serde(with = "ts_seconds")]
    pub regular_market_time:    DateTime<Utc>,
    pub scale:                  u8,
    pub symbol:                 String,
    pub timezone:               String,
    pub trading_periods:        Vec<Vec<TimeData>>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct YTHRCRMCTP {
    pub post:    TimeData,
    pub pre:     TimeData,
    pub regular: TimeData,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct TimeData {
    #[serde(with = "ts_seconds")]
    pub end:       DateTime<Utc>,
    pub gmtoffset: u32,
    #[serde(with = "ts_seconds")]
    pub start:     DateTime<Utc>,
    pub timezone:  String,
}
