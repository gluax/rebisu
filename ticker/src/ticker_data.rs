use std::fmt;

use chrono::{DateTime, Utc};
use mlua::UserData;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Default, Deserialize, PartialEq, Serialize)]
pub struct TickerData {
    pub close:  Vec<Option<f64>>,
    pub dates:  Vec<DateTime<Utc>>,
    pub high:   Vec<Option<f64>>,
    pub low:    Vec<Option<f64>>,
    pub open:   Vec<Option<f64>>,
    pub volume: Vec<Option<u64>>,
}

impl TickerData {
    pub fn is_empty(&self) -> bool {
        self.close.is_empty()
            || self.dates.is_empty()
            || self.high.is_empty()
            || self.low.is_empty()
            || self.open.is_empty()
            || self.volume.is_empty()
    }
}

impl fmt::Display for TickerData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:#?}", self)
    }
}

impl UserData for TickerData {
    fn add_fields<'lua, F: mlua::UserDataFields<'lua, Self>>(fields: &mut F) {
        fields.add_field_method_get("close", |_, this| Ok(this.close.clone()));
        fields.add_field_method_get("dates", |_, this| {
            Ok(this
                .dates
                .iter()
                .map(|d| d.to_string())
                .collect::<Vec<String>>())
        });
        fields.add_field_method_get("high", |_, this| Ok(this.high.clone()));
        fields.add_field_method_get("low", |_, this| Ok(this.low.clone()));
        fields.add_field_method_get("open", |_, this| Ok(this.open.clone()));
        fields.add_field_method_get("volume", |_, this| Ok(this.volume.clone()));
    }
}
