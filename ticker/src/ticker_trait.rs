use rebisu_common::TickerBackend;
use rebisu_configs::TickerSettings;
use rebisu_errors::Result;

use super::TickerData;

pub trait TickerTrait: Send + Sync + 'static {
    fn download(&self, auto_adjust_ohlc: Option<bool>, prepost: Option<bool>)
    -> Result<TickerData>;

    fn get_ticker_name(&self) -> &'static str;
    fn get_ticker_backend(&self) -> TickerBackend;

    fn get_settings(&self) -> &TickerSettings;
    fn set_settings(&mut self, settings: TickerSettings);

    fn validate_interval(&self) -> Result<()>;
    fn validate_period(&self) -> Result<()>;
}
