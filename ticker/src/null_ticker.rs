use rebisu_common::TickerBackend;
use rebisu_configs::TickerSettings;
use rebisu_errors::Result;

use super::{TickerData, TickerTrait};

#[derive(Clone, Debug, Default)]
pub struct NullTicker {
    ticker_name: &'static str,
    settings:    TickerSettings,
}

impl NullTicker {
    pub fn new(ticker_name: &'static str, settings: TickerSettings) -> Self {
        Self {
            ticker_name,
            settings,
        }
    }
}

impl TickerTrait for NullTicker {
    fn download(
        &self,
        _auto_adjust_ohlc: Option<bool>,
        _prepost: Option<bool>,
    ) -> Result<TickerData> {
        Ok(Default::default())
    }

    fn get_ticker_name(&self) -> &'static str {
        self.ticker_name
    }

    fn get_ticker_backend(&self) -> TickerBackend {
        TickerBackend::NullBackend
    }

    fn get_settings(&self) -> &TickerSettings {
        &self.settings
    }

    fn set_settings(&mut self, settings: TickerSettings) {
        self.settings = settings
    }

    fn validate_interval(&self) -> Result<()> {
        Ok(())
    }

    fn validate_period(&self) -> Result<()> {
        Ok(())
    }
}
