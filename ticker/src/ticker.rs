use bevy::prelude::Component;
use rebisu_common::TickerBackend;
use rebisu_configs::TickerSettings;
use rebisu_errors::{AppError, ErrorHandler, Result};

use super::{NullTicker, TickerData, TickerTrait, YFinance};

#[derive(Component)]
pub struct Ticker {
    inner: Box<dyn TickerTrait>,
}

impl Ticker {
    pub fn new(
        ticker_name: &'static str,
        settings: TickerSettings,
        handler: &mut ErrorHandler,
    ) -> Result<Self> {
        let inner: Box<dyn TickerTrait> = match settings.ticker_backend {
            TickerBackend::Yahoo => Box::new(YFinance::new(ticker_name, settings)?),
            TickerBackend::NullBackend => {
                handler.emit_err(AppError::invalid_ticker_backend(
                    &settings.ticker_backend,
                    ticker_name,
                ));

                Box::new(NullTicker::new(ticker_name, settings))
            }
        };

        Ok(Self { inner })
    }
}

impl TickerTrait for Ticker {
    fn download(
        &self,
        auto_adjust_ohlc: Option<bool>,
        prepost: Option<bool>,
    ) -> Result<TickerData> {
        self.inner.download(auto_adjust_ohlc, prepost)
    }

    fn get_ticker_name(&self) -> &'static str {
        self.inner.get_ticker_name()
    }

    fn get_ticker_backend(&self) -> TickerBackend {
        self.inner.get_ticker_backend()
    }

    fn get_settings(&self) -> &TickerSettings {
        self.inner.get_settings()
    }

    fn set_settings(&mut self, settings: TickerSettings) {
        self.inner.set_settings(settings)
    }

    fn validate_interval(&self) -> Result<()> {
        self.inner.validate_interval()
    }

    fn validate_period(&self) -> Result<()> {
        self.inner.validate_period()
    }
}
